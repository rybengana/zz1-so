#!/bin/bash

cat "$@" | {
    while IFS='\n' read line
    do 
        prefixe="$(echo "$line" | cut -d: -f1)"
        ligne="$(echo "$line" | cut -d: -f2-)"
        if [ $prefixe = "1" ]
        then 
            echo $ligne
        else
            echo $ligne 1>&2
        fi
    done 
}