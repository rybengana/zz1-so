#!/bin/bash

if test $# -ne 2
then 
    echo "Problème : Il faut exactement 2 arguments!" >&2
    echo $(exit) >&2
else 
    echo $2
    echo $1
fi 

