#!/bin/bash

#echo "$1! Vous avez dit $1?!"

if [ $# -eq 0 ]
then 
    echo "Rien? Vous ne dites rien?!"
else 
    for i in $(seq $#)
    do 
        echo "$1! Vous avez dit $1?!"
        shift
    done
fi 

