TP effectué le 21/05

# Arguments de scripts

## Q1

```bash
./exclamation.sh arg
```

## Q2

Renvoie 
```bash
9 10 1 2 3 4 5 6 7 8
```

Car le premier `./rotate10.sh $(seq 10)` a déjà modifié l'ordre des paramètres.


## Q6

On peut aussi faire avec : 

```bash
if test $# -eg 0
then 
    echo ...
fi
```

ou encore 

```bash
test $# -eq 0 &&echo ...
```

# Redirections

## Q1 

### 1
`read` : primitive shell. Renvoie le nombre de bytes lues, ou -1 en cas d'erreur. Equivalent du scanf. Elle va mettre dans la variable passé en argument une lecture au clavier.

### 2
`let i++`: let permet d'effectuer une opération. Dans notre cas, elle incrémente i.

### 3
Si le script est appelé sans arguments, alors pas de fin : tout ce qu'on écrit est stocké jusqu'à l'obtention d'un retour à ligne. Lorsque que l'on fait retour à la ligne, le programme réaffiche ce que nous avons écrit (avec au début le numéro de ligne) et attend une nouvelle saisie.

### 4
```bash
./numcat.sh fichier_lisible.txt
```

Affiche le contenu du fichier, en numérotant les lignes

### 5 
Si il y a plusieurs arguments, alors il affiche d'abord le contenu du 1er fichier, ainsi que celui du 2e fichier. Par contre la numérotation des lignes n'est pas mise à 0 entre les lectures des 2 fichiers.

## Q2

Il n'y a aucune différence entre le script et la commande `cat -n`. Les deux font la même chose (affichage différent).

## Q3

`cut -d` : sépare les chaînes de caractères en plusieurs champs, avec `-d :` définissant `:` comme délimiteur. `-f2` renvoie seulement la chaîne à partir du 2e champs (jusqu'à la fin de la ligne).

## Q4

```bash
num="$(echo "$PATH" | cut -d: -f1)"
```

On met dans la variable num le résultat de l'opération, qui nous renvoie le 1er champ (délimité par `:`) du résultat de `echo "$PATH"`. Donc ici : `/opt/oracle/instantclient_19_6`

## Q5

Voir fichier tri.sh

## Q6 

### Q1

Compare les lignes des 2 fichiers.

### Q2

Ne marche pas (renvoie une erreur artificielle, prévue dans le cas où il n'y a pas 2 arguments).

### Q3 et Q4

Erreur : fichier non lisible ou n'existe pas.

## Q10

Son type est "une fonction". La commande type nous donne aussi le corps de la fonction.

## Q11

Renvoie la même chose que si pas de paramètres (normal?).