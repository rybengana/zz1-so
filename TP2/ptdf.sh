#!/bin/bash

function pfdt(){
    if [ $# -eq 0 ]
    then
        echo $$
        ls -g --time-style=+/proc/$$/fd/
    else 
        for i in $(seq $#)
        do
            echo $1
            ls -g --time-style=+/proc/$1/fd/
            shift
        done
    fi
}