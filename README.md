# ZZ1 - Système d exploitation

[lien du cours](https://ent.uca.fr/moodle/course/view.php?id=16577#section-3)

# Organisation des séances 

## Cours amphi 

### Cours 1 : Introduction (12/03/24)

### Cours 2 : Processus (19/03/24)

### Cours 3 : Gestion de la Mémoire (09/04/24)

### Cours 4 : Parallélisme, Concurrence et Fichiers (29/04/24)

### Cours 5 : (29/05/24)

### Cours 6 : (04/06/24)

## Travaux Pratiques

### TP 1 : Commandes Simples, Partie 1 (07/05/24)

### TP2 : Scripts (21/05/24)

### TP3 et TP4 : (28/05/24)
