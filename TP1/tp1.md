# Commandes Simples

TP effectué le 19/03 (en autonomie)


## Substitutions

### Q1

```bash
echo ~
``` 

nous renvoie `/home/ryadh` (dossier personnel)

### Q2

```bash
echo *
```

Renvoie tout ce qui est présent dans le répertoire courant.

### Q3

```bash
echo .*
```

Lorsqu'on ajoute le symbole `*` au milieu d'une chaine, celle-ci peut prendre comme valeur n'importe quelle chaîne de caractères. Ainsi, `.*`, par exemple, nous renvoie l'ensemble des mots qui commencent par un `.`

### Q4

```bash
NIMPORTEQUOIX='pipou'
echo $NIMPORTEQUOIX
```

Le symbole `$` remplace une 'variable' (ou un alias) par sa valeur réelle. Ainsi, dans notre cas, la commande renvoie `pipou`.

### Q5

```bash
echo $RANDOM
```

Cette commande nous renvoie un nombre aléatoire différent à chaque exécution.


### Q6

```bash
echo {a,b,c}
```

Nous renvoie la liste de caractères : `a b c`.
Si on l'insère à l'intérieur d'un mot, comme par exemple :

```bash
echo pipou{a,b,c}
```

Cela nous renvoie : `pipoua pipoub pipouc`.

### Q7

```bash
echo {a/{0,1/},b/0}/{x,y}
```

Génère une expansion de listes. Renvoie exactement : `a/0/x a/0/y a/1//x a/1//y b/0/x b/0/y`

### Q8

```bash
echo {a}
```

Non, le mot `{a}` n'est pas substituté. Cela nous renvoie `{a}`.

### Q9

```bash
echo $(echo *)
```

Renvoie les fichiers présent dans le répertoire courant. Substitue le résultat de la commande.

```bash
echo $(ls)
```

Même résultat qu'au dessus.

```bash
echo $(pwd)
```

Même résultat que `pwd`.

```bash
echo $(seq 3 8)
```

Renvoie `3 4 5 6 7 8`.

### Q10

```bash
echo "********** faisons une coupure."
```

### Q11

```bash
echo "Utilisez '~login' pour indiquer le home de l'utilisateur 'login'."
```

> [NOTE]
> Ici, la commande avec de simple quotes `'` ne marcherait pas, puisque nous avons également ce symbole dans la chaîne de caractères à afficher.

### Q12

```bash
echo "\$HOME est remplacé par le chemin vers le  home de l'utilisateur courant."
```

> ![IMPORTANT]
> Cette même commande avec de simple quote `'` ne marcherait pas. De même pour la commande sans le caractère `\` devant `$HOME`, i.e `"$HOME est remplacé par le chemin vers le  home de l'utilisateur courant."`, puisque `$HOME` serait substitué par sa valeur.

### Q13

- `'` : Ne fait pas de substitution.

- `"` : Fait les substitutions:

- `$` : variable d'environnement
- `*`
- `~`
- `{}` : liste de mots
- `$(...)` : commande


## Découpage en mot 

### Q1 

Renvoie une seule ligne résultat

```bash
printf '%s\n' $NIMPORTEQUOIX
```

Renvoie le contenu sur 2 lignes : une ligne par mot.

```bash
printf '%s\n' {a,b,c}
```

Une ligne par lettre.

```bash
printf '%s\n' {a,b,c}{a,b,c}
```

Renvoie une combinaison formée à partir d'un élément de chaque ensemble, séparée ligne par ligne.


```bash
printf '%s\n' $(seq 3 8)
```

Renvoie la séquence d'entiers en 3 et 8 (inclus), un élément par ligne.

```bash
printf '%s\n' "$(seq 3 8)"
```

Même résultat.

### Q2

```bash
printf '%s\n' Bonjour\ monsieur\ . Aurevoir\ monsieur\ .
```

ou 

```bash
printf '%s\n' "Bonjour monsieur ." "Aurevoir monsieur ."
```

### Q3

```bash
printf '%s\n' Bonjour\ monsieur\ "$USER". Aurevoir\ monsieur\ "$USER".
```

ou 

```bash
printf '%s\n' "Bonjour monsieur $USER." "Aurevoir monsieur $USER."
```

### Q4

```bash
printf '%s\n' {Bonjour,Aurevoir}" monsieur $USER"
```

## Commande

### Q1

```bash
type cd
```

Renvoie `cd is a shell builtin`.

### Q2

```bash
type echo
```

Renvoie `echo is a shell builtin` : primitive du shell.

### Q3

```bash
which echo
```

Renvoie la localisation de la commande `echo`, à savoir : `/usr/bin/echo`.

### Q4

Oui, il existe un `/bin/echo`.

```bash
cat /bin/echo
```

La priorité entre les builtin BASH et les primitives shell dépend des OS.

### Q5

Primitive Shell 

### Q6 

C'est un alias 

## Alias

### Q2 

```bash
alias printarg="printf '%s\n'"
```

### Q3



## Scripts 

### Q1

Ne pas oublier d'ajouter les droits d'execution. 
```bash
chmod +x helloworld.sh
```

puis 

```bash
./helloworld.sh
```

### Q2 

Utiliser cat nous affiche le contenu du fichier (à savoir `echo Hello World!`).

### Q3 

On met en entête du fichier : `#!/bin/bash`. 

### Q5

Si on fait `./helloworld.sh` ou `bash helloworld.sh`, la variable d'environnement n'est pas sauvegardée dans le shell courant (car créé un processus fils pour executer le programme). Alors qu'avec `source helloworld.sh`, on peut réutiliser la variable d'environnement.


## Tubes Simples

### Q1

affiche ce que l'on écrit après chaque entrée.

### Q2 

`grep a` renvoie toutes les chaînes de caractères qu'il prend en argument qui contiennent le caractère `a`.

### Q3 

`... | bash` tente d'exécuter le résultat de la commande qui précère le `|`.

### Q4

```bash
cat helloworld.sh | bash
```

### Q5

```bash
source helloworld.sh | tr 'Wrd!' 'co! '
```

`tr 'Wrd!' 'co! '` remplace les caractères du 1er argument par les caractères du 2nd argument.

## Appels système

### Q1

`strace`: trace et renvoie les appels systèmes.

### Q2

arguments : "Hello %s" et "rybengana"
Le remplacement de $USER est fait avant l'appel de fonction.



